#include "game_logic.h"
#include "protocol.h"
#include "GameState.h"
#include "Track.h"

#include "debug_draw.h"

using namespace hwo_protocol;

game_logic::game_logic(Bot& bot)
	: action_map
{
	{ "join", &game_logic::on_join },
	{ "yourCar", &game_logic::on_your_car },
	{ "gameInit", &game_logic::on_game_init },
	{ "gameStart", &game_logic::on_game_start },
	{ "carPositions", &game_logic::on_car_positions },
	{ "crash", &game_logic::on_crash },
	{ "spawn", &game_logic::on_spawn },
	{ "gameEnd", &game_logic::on_game_end },
	{ "error", &game_logic::on_error }
}, bot(bot), currentTick(0), previousGameStateTick(-1)
{
}

game_logic::~game_logic() { }

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
	const auto& msg_type = msg["msgType"].as<std::string>();
	const auto& data = msg["data"];
	if(msg.has_member("gameTick")) {
		currentTick = msg["gameTick"].as_int();
	}
	auto action_it = action_map.find(msg_type);
	if (action_it != action_map.end())
	{
		return (action_it->second)(this, data);
	}
	else
	{
		std::cout << "Unknown message type: " << msg_type << std::endl;
		return { make_ping() };
	}
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
	std::cout << "Joined" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
	myColor = data["color"].as_string();
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
	std::cout << "Game initialized" << std::endl;
	track = Track::fromJSON(data["race"]["track"]);
	bot.initGame(track);
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
	std::cout << "Race started" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	GameState gameState = GameState::fromJSON(data, track);
	for(CarState& carState : gameState.cars) {
		carState.crashed = (bool)crashedColors.count(carState.color);
	}
	
	if(previousGameStateTick != -1) {
		double tickdiff = currentTick - previousGameStateTick;
		if(tickdiff == 0) tickdiff = 1;
		
		for(CarState& carState : gameState.cars) {
			int previousCarIndex = previousGameState.getCarIndexByColor(carState.color);
			CarState& previousCarState = previousGameState.cars[previousCarIndex];
			carState.velocity = track.getDistance(
				carState.fromLane,
				previousCarState.piece, previousCarState.inPieceDistance,
				carState.piece, carState.inPieceDistance
			) / tickdiff;
			carState.acceleration = (carState.velocity - previousCarState.velocity) / tickdiff;
		}
	}
	previousGameStateTick = currentTick;
	previousGameState = gameState;
	
#ifdef DEBUG_DRAW
	drawGameState(track, gameState);
#endif
	Bot::Move move = bot.run(gameState, gameState.getCarIndexByColor(myColor));
	return { make_throttle(move.throttle) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	std::cout << "Someone crashed" << std::endl;
	crashedColors.insert(data["color"].as_string());
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
	std::cout << "Someone spawned" << std::endl;
	crashedColors.erase(data["color"].as_string());
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
	std::cout << "Race ended" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
	std::cout << "Error: " << data.to_string() << std::endl;
	return { make_ping() };
}
