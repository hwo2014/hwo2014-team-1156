#pragma once

#include "Vector.hpp"

template<class A>
struct Assign {};

template<class T, int N>
struct Matrix {
	Vector<T,N> columns[N];
	Vector<T,N>& operator[](int i) { return columns[i]; }
	const Vector<T,N>& operator[](int i) const { return columns[i]; }

	T* ptr() { return &columns[0][0]; }
	const T* ptr() const { return &columns[0][0]; }

	template<class A>
	Matrix& operator=(const Assign<A>& a) {
		static_cast<const A&>(a)(*this);
		return *this;
	}
	Matrix() {}
	template<class A>
	Matrix(const Assign<A>& a) {
		static_cast<const A&>(a)(*this);
	}

	void transpose() {
		for(int i=0; i<N-1; ++i)
			for(int j=i+1; j<N; ++j)
				swap(columns[i][j], columns[j][i]);
	}
};

typedef Matrix<Real, 4> Matrix4;
typedef Matrix<Real, 3> Matrix3;
typedef Matrix<Real, 2> Matrix2;

#define OP(op,ope)\
	template<class T, int N> inline Matrix<T,N>& operator ope(Matrix<T,N>& a, const Matrix<T,N>& b) { for(int i=0; i<N; ++i) a[i] ope b[i]; return a; }\
	template<class T, int N> inline Matrix<T,N> operator op(Matrix<T,N> a, const Matrix<T,N>& b) { return a ope b; }
OP(+,+=)
OP(-,-=)
#undef OP

#define OP(op,ope)\
	template<class T, int N> inline Matrix<T,N>& operator ope(Matrix<T,N>& a, T x) { for(int i=0; i<N; ++i) a[i] ope x; return a; }\
	template<class T, int N> inline Matrix<T,N> operator op(Matrix<T,N> a, T x) { return a ope x; }
OP(*,*=)
OP(/,/=)
#undef OP

struct Zero: Assign<Zero> {
	template<class T, int N>
	void operator()(Matrix<T,N>& m) const {
		for(int i=0; i!=N; ++i)
			for(int j=0; j!=N; ++j)
				m[i][j] = 0;
	}
};
struct Identity: Assign<Identity> {
	template<class T, int N>
	void operator()(Matrix<T,N>& m) const {
		m = Zero();
		for(int i=0; i<N; ++i) m[i][i] = 1;
	}
};
template<class T, int N>
struct Translate: Assign<Translate<T,N> > {
	Translate(Vector<T,N> v): v(v) {}
	Vector<T,N> v;
	void operator()(Matrix<T,N>& m) const {
		m = Identity();
		for(int i=0; i<N; ++i) m[N-1][i] = v[i];
	}
};
template<class T, int N>
struct Scale: Assign<Scale<T,N> > {
	Scale(Vector<T,N> v): v(v) {}
	Vector<T,N> v;
	void operator()(Matrix<T,N>& m) const {
		m = Zero();
		for(int i=0; i<N; ++i) m[i][i] = v[i];
	}
};

template<class T, int N>
Translate<T,N> translateMatrix(Vector<T,N> v) {
	return {v};
}
#if 0
template<class...A>
Translate<sizeof...(A)> translateMatrix(A... args) {
	return Translate<sizeof...(A)>(Vector<float,sizeof...(A)>(args...));
}
#endif
template<class T, int N>
Scale<T,N> scaleMatrix(Vector<T,N> v) {
	return {v};
}
#if 0
template<class...A>
Scale<sizeof...(A)> scaleMatrix(A... args) {
	return Scale<sizeof...(A)>(Vector<float,sizeof...(A)>(args...));
}

// TODO: more generic
template<class...A>
Matrix4 initMatrix(A... args) {
	static_assert(sizeof...(A)==16, "Wrong number of initializer arguments.");
	Matrix4 m;
	assign(m.ptr(), args...);
	return m;
}
#endif

template<class T, int N>
Matrix<T,N> transpose(Matrix<T,N> m) { m.transpose(); return m; }


template<class T, int N>
Matrix<T,N> operator*(const Matrix<T,N>& a, const Matrix<T,N>& b) {
	Matrix<T,N> r = Zero();
	for(int i=0; i<N; ++i)
		for(int j=0; j<N; ++j)
			r[i] += a[j] * b[i][j];
	return r;
}

template<class T, int N>
Vector<T,N> operator*(const Matrix<T,N>& a, const Vector<T,N>& v) {
	Vector<T,N> r = Vector<T,N>::zero();
	for(int i=0; i<N; ++i)
		r += v[i]*a[i];
	return r;
}
