#pragma once
#include "Matrix.hpp"

/** Represents the transform v -> M*v + u for some matrix M and vector u. */
template<class T, int N>
struct AffineTransform {
	Matrix<T,N> matrix;
	Vector<T,N> vector;

	static constexpr AffineTransform translate(const Vector<T,N>& v) {
		return {Identity(), v};
	}
};

typedef AffineTransform<Real,2> Transform2;

template<class T, int N>
inline AffineTransform<T,N> operator*(const AffineTransform<T,N>& a, const AffineTransform<T,N>& b) {
	return AffineTransform<T,N>{a.matrix * b.matrix, a.matrix * b.vector + a.vector};
}

template<class T, int N>
inline Vector<T,N> operator*(const AffineTransform<T,N>& a, const Vector<T,N>& v) {
	return a.matrix * v + a.vector;
}

template<class T, int N>
inline AffineTransform<T,N> translateTransform(const Vector<T,N>& v) {
	return {Identity(), v};
}

template<class T, int N>
inline AffineTransform<T,N> matrixTransform(const Matrix<T,N>& m) {
	return {m, Vector<T,N>::zero()};
}

template<class T, int N>
inline AffineTransform<T,N> scaleTransform(const Vector<T,N>& v) {
	return matrixTransform(Matrix<T,N>{scaleMatrix(v)});
}
