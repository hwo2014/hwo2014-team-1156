#include "Track.hpp"
#include "GameState.hpp"
#include "AffineTransform.hpp"
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <iostream>
using namespace std;

namespace {

Matrix2 rotationMatrix(double rotation) {
	Real c = cos(rotation);
	Real s = sin(rotation);
	Matrix2 res;
	res[0][0] = c;
	res[0][1] = s;
	res[1][0] = -s;
	res[1][1] = c;
	return res;
}

struct Rectangle {
	Real bounds[2][2]; // [x/y][from/to]

	Real* operator[](int i) { return bounds[i]; }
	const Real* operator[](int i) const { return bounds[i]; }

};
const Rectangle emptyRect{{{1e100,-1e100},{1e100,-1e100}}};
Rectangle rectUnion(Rectangle a, Rectangle b) {
	for(int i=0; i<2; ++i) {
		a[i][0] = min(a[i][0], b[i][0]);
		a[i][1] = max(a[i][1], b[i][1]);
	}
	return a;
}
Rectangle pointRec(Vec2 v) {
	return {{{v[0],v[0]},{v[1],v[1]}}};
}
Rectangle offsetRect(Rectangle r, Real offset) {
	for(int i=0; i<2; ++i) {
		r[i][0] -= offset;
		r[i][1] += offset;
	}
	return r;
}
#if 0
Rectangle fromSF(sf::FloatRect rect) {
	Rectangle res;
	res[0][0] = rect.left;
	res[0][1] = rect.left + rect.width;
	res[1][0] = rect.top;
	res[1][1] = rect.top + rect.height;
	return res;
}
#endif
Rectangle bounds(const vector<Vec2>& vec) {
	Rectangle res = emptyRect;
	for(Vec2 v: vec) {
		res = rectUnion(res, pointRec(v));
	}
	return res;
}

Transform2 rectangleToUnitBox(Rectangle r) {
	auto cornerToOrigin = translateTransform(-Vec2{r[0][0], r[1][0]});
	Vec2 scale{1 / (r[0][1] - r[0][0]), 1 / (r[1][1] - r[1][0])};
	auto scaleToUnit = scaleTransform(scale);
	return scaleToUnit * cornerToOrigin;
}

Transform2 rectangleToWindowTransform(Rectangle r, double w, double h) {
	return scaleTransform(Vec2{w,-h})
		* translateTransform(Vec2{0,-1})
		* rectangleToUnitBox(r);
}

sf::Vector2f toSF(Vec2 v) {
	return sf::Vector2f(v[0], v[1]);
}

sf::VertexArray laneToArray(const vector<Vec2>& p) {
	assert(!p.empty());
	sf::VertexArray res(sf::LinesStrip, p.size()+1);
	for(size_t i=0; i<=p.size(); ++i)
		res[i] = toSF(p[i%p.size()]);
	return res;
}

sf::Transform toSFTransform(Transform2 transform) {
	const auto& m = transform.matrix;
	const auto& v = transform.vector;
	return sf::Transform(
		m[0][0], m[1][0], v[0],
		m[0][1], m[1][1], v[1],
		0, 0, 1);
}

#if 0
Transform2 carTransform(const CarState& car) {
	constexpr double sizeW = 1, sizeH = 2;
	Transform2 rotation = matrixTransform(rotationMatrix(car.rotation));
	return translateTransform(car.position)
		* scaleTransform(Vec2{sizeW, sizeH})
		* rotation
		* translateTransform(Vec2{-.5, -.5});
}
#endif

sf::RenderWindow window;

Transform2 arcTransform(Real rad, Real angle) {
	if (angle > 0) {
		Vec2 dir = angleToVec(angle);
		dir[0] *= -1;
		Vec2 pos = rad * (Vec2(1,0) + dir);
		return Transform2{rotationMatrix(-angle), pos};
	} else {
		Vec2 dir = angleToVec(-angle);
		Vec2 pos = rad * (Vec2(-1,0) + dir);
		return Transform2{rotationMatrix(-angle), pos};
	}
}
Transform2 pieceTransform(Piece piece, Real offset, Real pos=1) {
	if (piece.type == Piece::STRAIGHT) {
		return translateTransform(Vec2(0,piece.straight.length * pos));
	} else {
		Real rad = piece.bend.radius + offset;
		return arcTransform(rad, piece.bend.angle * pos);
	}
}

vector<Transform2> makePieceTransforms(const vector<Piece>& pieces, Real offset) {
	vector<Transform2> res;
	Transform2 transform{Matrix2(Identity()), Vec2(offset,0)};
	for(Piece piece: pieces) {
		res.push_back(transform);
		transform = transform * pieceTransform(piece, offset);
	}
	return res;
}

vector<Vec2> traceLane(const vector<Piece>& pieces, Real offset) {
	vector<Vec2> res;
	Transform2 transform{Matrix2(Identity()), Vec2(offset,0)};
	for(Piece piece: pieces) {
		res.push_back(transform.vector);
		if (piece.type == Piece::STRAIGHT) {
			transform = transform * translateTransform(Vec2(0,piece.straight.length));
		} else {
			Real rad = piece.bend.radius + offset;
			int n=10;
			for(int i=1; i<n; ++i) {
				Real ang = piece.bend.angle * i / n;
				res.push_back(transform * arcTransform(rad, ang).vector);
			}
			transform = transform * arcTransform(rad, piece.bend.angle);
		}
	}
	res.push_back(transform.vector);
	return res;
}

}

void initDraw() {
	window.create(sf::VideoMode(1024, 768), L"aina häviää");
}

void handleFrame(const Track& track, GameState state) {
	if (!window.isOpen()) {
		exit(0);
	}
	sf::Event event;
	while(window.pollEvent(event)) {
		if (event.type == sf::Event::Closed) window.close();
	}

	window.display();
	window.clear();

	Rectangle area = emptyRect;
	vector<vector<Transform2>> pieceTransforms;
	vector<vector<Vec2>> lanes;
	for(Real offset: track.lanes) {
		lanes.push_back(traceLane(track.pieces, offset));
		area = rectUnion(area, bounds(lanes.back()));
		pieceTransforms.push_back(makePieceTransforms(track.pieces, offset));
	}
	area = offsetRect(area, 10);
//	cout<<"area "<<area[0][0]<<" - "<<area[0][1]<<" ; "<<area[1][0]<<" - "<<area[1][1]<<'\n';
	Transform2 windowTransform = rectangleToWindowTransform(area,
			window.getSize().x, window.getSize().y);
//	cout<<"win "<<window.getSize().x<<' '<<window.getSize().y<<'\n';
	for(const auto& lane: lanes) {
//		cout<<"drawing lane:\n";
//		for(Vec2 v: lane) cout<<v<<' ';
//		cout<<'\n';
		sf::VertexArray array = laneToArray(lane);
		window.draw(array, toSFTransform(windowTransform));
	}
#if 1
	for(const CarState& car: state.cars) {
//		cout<<"car "<<car.piece<<' '<<car.inPieceDistance<<'\n';
//		Transform2 transform = windowTransform * carTransform(car);
		Real offset = track.lanes[car.fromLane];
		Transform2 transform = windowTransform
			* pieceTransforms[car.fromLane][car.piece]
			* pieceTransform(track.pieces[car.piece], offset, car.inPieceDistance / track.pieces[car.piece].size(offset));
		sf::RectangleShape shape({1,1});
		window.draw(shape, toSFTransform(transform));
	}
#endif
	window.display();
}
