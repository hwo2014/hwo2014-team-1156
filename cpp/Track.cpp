#include "Track.h"

#include <cassert>
#include <cmath>

constexpr static double PI = 4 * std::atan(1);

Piece Piece::fromJSON(const jsoncons::json& src) {
	Piece ret;
	ret.canSwitch =
		src.has_member("switch") && src["switch"].as_bool();
	if(src.has_member("length")) {
		ret.type = Piece::STRAIGHT;
		ret.straight.length = src["length"].as_double();
	} else {
		ret.type = Piece::BEND;
		ret.bend.radius = src["radius"].as_double();
		ret.bend.angle = PI * src["angle"].as_double() / 180.0;
	}
	return ret;
}

Lane Lane::fromJSON(const jsoncons::json& src) {
	Lane ret;
	ret.distanceFromCenter = src["distanceFromCenter"].as_double();
	ret.server_index = src["index"].as_int();
	return ret;
}

Track Track::fromJSON(const jsoncons::json& src) {
	Track ret;
	
	auto& lanes = src["lanes"];
	auto& pieces = src["pieces"];
	
	for(auto it = lanes.begin_elements(); it != lanes.end_elements(); ++it) {
		ret.lanes.push_back(Lane::fromJSON(*it));
	}
	sort(
		ret.lanes.begin(), 
		ret.lanes.end(),
		[](Lane a, Lane b) {
			auto pair1 = std::make_pair(a.distanceFromCenter, a.server_index);
			auto pair2 = std::make_pair(b.distanceFromCenter, b.server_index);
			return pair1 < pair2;
		}
	);
	
	for(auto it = pieces.begin_elements(); it != pieces.end_elements(); ++it) {
		ret.pieces.push_back(Piece::fromJSON(*it));
	}
	
	return ret;
}

int Track::getLaneIndexByServerIndex(int server_index) const {
	for(int lanei = 0; lanei < (int)lanes.size(); ++lanei) {
		if(lanes[lanei].server_index == server_index) return lanei;
	}
	assert(false);
	return -1;
}

double Track::getLanePieceLength(int lane, int piece) const {
	double offset = lanes[lane].distanceFromCenter;
	if(pieces[piece].type == Piece::BEND && pieces[piece].bend.angle > 0) {
		offset = -offset;
	}
	return pieces[piece].size(offset);
}

double Track::getLanePieceInverseSignedRadius(int lane, int piece) const {
	if(pieces[piece].type == Piece::STRAIGHT) return 0.0;
	
	double offset = lanes[lane].distanceFromCenter;
	double signedRadius = pieces[piece].bend.radius;
	if(pieces[piece].bend.angle > 0) {
		signedRadius = -signedRadius;
	}
	
	return 1.0 / (signedRadius + offset);
}

double Track::getDistance(int lane, int piece1, double pos1, int piece2, double pos2) const {
	if(piece1 == piece2 && pos1 <= pos2) {
		return pos2 - pos1;
	}
	
	double dist = getLanePieceLength(lane, piece1) - pos1;
	int piece = piece1 + 1;
	if(piece == (int)pieces.size()) piece = 0;
	while(piece != piece2) {
		dist += getLanePieceLength(lane, piece);
		++piece;
		if(piece == (int)pieces.size()) piece = 0;
	}
	dist += pos2;
	
	return dist;
}
