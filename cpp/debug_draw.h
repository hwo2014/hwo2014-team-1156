#pragma once

#include "GameState.h"
#include "Track.h"

#ifdef DEBUG_DRAW

// Draw game state, opening a window if none has been opened.
void drawGameState(const Track& track, const GameState& gameState);

// Get the current throttle inputted by user.
double getInteractiveThrottle();

#endif
