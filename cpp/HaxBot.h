#pragma once
#include "Bot.h"
#include "GameState.h"

class HaxBot: public Bot {
public:
	virtual void initGame(const Track& track) override {
		this->track = track;
	}
	virtual Move run(const GameState& gameState, int myIndex) override {
		const CarState& carState = gameState.cars[myIndex];
		double throttle = 0.7;
//		double angle = carState.angle;
//		std::cerr << carState.velocity << " " << carState.acceleration / std::cos(angle) / throttle << "\n";
		return {throttle, 0};
	}
	
private:
	Track track;
};
