#pragma once

#include "Track.h"
#include "Bot.h"

#include <memory>
#include <vector>

// Run a game on a bot with physics that are a good approximation of the
// physics on the real server. (TODO: make it good)
void runFakeServer(const std::vector<std::unique_ptr<Bot>>& bots, const Track& track);
