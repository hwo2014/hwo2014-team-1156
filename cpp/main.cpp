#include <iostream>
#include <fstream>
#include <string>
#include <jsoncons/json.hpp>
#include "protocol.h"
#include "connection.h"
#include "game_logic.h"
#include "HaxBot.h"
#include "InteractiveBot.h"
#include "fake_server.h"

using namespace hwo_protocol;

// If msg_log != nullptr, all messages are written to it. Each message is a
// JSON object with two fields:
//  - query: Query object from the server.
//  - responses: Array of response objects from the client.
void run(hwo_connection& connection, Bot& bot, std::ostream* msg_log) {
	game_logic game(bot);
	for (;;)
	{
		boost::system::error_code error;
		auto response = connection.receive_response(error);

		if (error == boost::asio::error::eof)
		{
			std::cout << "Connection closed" << std::endl;
			break;
		}
		else if (error)
		{
			throw boost::system::system_error(error);
		}

		auto client_response = game.react(response);

		if(msg_log) {
			jsoncons::json log_event;
			log_event["query"] = response;
			log_event["response"] = jsoncons::json(client_response.begin(), client_response.end());
			*msg_log << log_event.to_string() << "\n";
			msg_log->flush();
		}

		connection.send_requests(std::move(client_response));
	}
}

std::unique_ptr<Bot> chooseBotOrFail(const std::string& botId) {
	std::unique_ptr<Bot> bot;

	if(botId == "default") bot.reset(new HaxBot);
	if(botId == "interactive") bot.reset(new InteractiveBot);

	if(!bot) {
		std::cerr << "Unknown bot id \"" << botId << "\".\n";
		exit(1);
	}
	
	return bot;
}

void printUsage() {
	std::cerr << "Usage: ./run host port botname botkey [bot_id] [message_log_output]" << std::endl;
	std::cerr << "or     ./run -local track [bot_id1] [bot_id2] ..." << std::endl;
}

int main(int argc, const char* argv[])
{
	if(argc < 2) {
		printUsage();
		return 1;
	}
	
	const std::string host(argv[1]);
	
	if(host == "-local") {
		if(argc < 3) {
			printUsage();
			return 1;
		}
		
		const std::string trackName(argv[2]);
		jsoncons::json trackJSON = jsoncons::json::parse_file(trackName);
		Track track = Track::fromJSON(trackJSON);
		
		std::vector<std::unique_ptr<Bot>> bots;
		for(int i = 3; i < argc; ++i) {
			std::string botId = argv[i];
			bots.push_back(chooseBotOrFail(botId));
		}
		
		runFakeServer(bots, track);
	} else {
		if (argc < 5 || argc > 7)
		{
			printUsage();
			return 1;
		}

		const std::string port(argv[2]);
		const std::string name(argv[3]);
		const std::string key(argv[4]);
		std::cout << "Host: " << host << ", port: " << port << ", name: " << name << ", key:" << key << std::endl;

		std::string bot_id = "default";
		if(argc > 5) bot_id = argv[5];
		std::unique_ptr<Bot> bot = chooseBotOrFail(bot_id);

		std::unique_ptr<std::ofstream> msg_log;
		if(argc > 6) {
			msg_log.reset(new std::ofstream);
			msg_log->exceptions(std::ofstream::eofbit | std::ofstream::badbit);
			msg_log->open(argv[6]);
		}

		hwo_connection connection(host, port);

		jsoncons::json init_msg = make_join(name, key);
		connection.send_requests({ init_msg });

		run(connection, *bot, msg_log.get());

		if(msg_log) {
			msg_log->close();
		}
	}
}
