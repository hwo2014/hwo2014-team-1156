#pragma once
#include "FunctionApproximator.h"
#include <vector>
#include <valarray>

class NeuralNetwork: public FunctionApproximator {
public:
	Vector evaluate(Vector input) override;
	void teach(Vector input, Vector output, double learningRate) override;

	static NeuralNetwork createRandom(std::vector<int> layerSizes);

private:
	NeuralNetwork() {}
//	std::vector<int> layerSizes;
	typedef std::vector<Vector> Matrix;
	std::vector<Matrix> weights;

	static Vector multMatVec(const Matrix& m, const Vector& v);
	template<class R>
	static Matrix randomMatrix(int from, int to, R& r);
	static double activation(double x);
	static double activationDerivate(double x);
};
