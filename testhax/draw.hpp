#pragma once
struct Track;
struct GameState;
void initDraw();
void handleFrame(const Track& track, GameState state);
