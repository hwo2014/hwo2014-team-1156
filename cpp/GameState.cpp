#include "GameState.h"
#include "pi.h"

#include <cassert>

CarState CarState::fromJSON(const jsoncons::json& src, const Track& track) {
	CarState res;
	auto& id = src["id"];
	res.name = id["name"].as_string();
	res.color = id["color"].as_string();
	res.angle = PI * src["angle"].as_double() / 180.0;
	auto& piece = src["piecePosition"];
	res.piece = piece["pieceIndex"].as_int();
	res.inPieceDistance = piece["inPieceDistance"].as_double();
	auto& lane = piece["lane"];
	res.fromLane = track.getLaneIndexByServerIndex(lane["startLaneIndex"].as_int());
	res.toLane = track.getLaneIndexByServerIndex(lane["endLaneIndex"].as_int());
	res.lap = piece["lap"].as_int();
	return res;
}

GameState GameState::fromJSON(const jsoncons::json& src, const Track& track) {
#if 1
	GameState res;
	for(auto it = src.begin_elements(); it != src.end_elements(); ++it) {
		res.cars.push_back(CarState::fromJSON(*it, track));
	}
	return res;
#endif
}

int GameState::getCarIndexByColor(const std::string& color) const {
	for(int i = 0; i < (int)cars.size(); ++i) {
		if(cars[i].color == color) return i;
	}
	assert(false);
	return 0;
}
