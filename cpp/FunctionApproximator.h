#pragma once
#include <valarray>

class FunctionApproximator {
public:
	typedef std::valarray<double> Vector;

	virtual Vector evaluate(Vector input) = 0;
	virtual void teach(Vector input, Vector output, double learningRate) = 0;
};
