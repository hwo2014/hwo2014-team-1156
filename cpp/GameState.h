#pragma once

#include "Track.h"

#include <vector>
#include <string>
#include <jsoncons/json.hpp>

struct CarState {
	/* Values from server */
	std::string name;
	std::string color;
	double angle = 0;
	double inPieceDistance = 0;
	int piece = 0;
	int fromLane = 0;
	int toLane = 0;
	int lap = 0;

	/* Computed/maintained values */
	bool crashed = false;
	double velocity = 0;
	double acceleration = 0;
#if 0
	double laneChangeState = 0;
	double angularVelocity = 0;
#endif
	
#if 0
	/* Values decided by bot. */
	double throttle = 0;
	int switchLane = 0;
#endif

	static CarState fromJSON(const jsoncons::json& src, const Track& track);
};

struct GameState {
	std::vector<CarState> cars;
	
	int getCarIndexByColor(const std::string& color) const;
	
	static GameState fromJSON(const jsoncons::json& src, const Track& track);
};
