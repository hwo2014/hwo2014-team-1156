#pragma once
struct GameState;
struct Track;

class Bot {
public:
	virtual ~Bot() {}

	struct Move {
		/** -1/0/1 for whether to change left, keep same or change right. */
		double throttle;
		int laneChange;
	};

	virtual void initGame(const Track& track) = 0;
	virtual Move run(const GameState& gameState, int myIndex) = 0;
};
