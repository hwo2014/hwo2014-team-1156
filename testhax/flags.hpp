#pragma once
#include <sstream>

namespace flag {
	struct ParserBase {
		virtual void parse(std::istream& in) = 0;
	};
	void addFlagParser(const char* name, ParserBase* parser);
	template<class T>
	struct Parser: ParserBase {
		T& value;
		void parse(std::istream& in) override {
			in >> value;
		}
		Parser(T& value, const char* name): value(value) {
			addFlagParser(name, this);
		}
	};

	void parseFlags(int argc, char* argv[]);
}

#define FLAG(name, type, defaultValue) \
	namespace flag { \
		type name((defaultValue)); \
		Parser<type> name ## _parser(name, #name); \
	}

