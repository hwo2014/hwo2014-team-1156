#pragma once

#include <cmath>

constexpr double PI = 4.0 * std::atan(1.0);
