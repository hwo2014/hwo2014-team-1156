#include "debug_draw.h"

#ifdef DEBUG_DRAW

#include "Vec2.h"
#include "pi.h"

#include "SDL.h"
#include "SDL_gfxPrimitives.h"

#include <cstdlib>
#include <functional>
#include <map>

namespace {

SDL_Surface* screen = nullptr;
double interactiveThrottle = 0.0;
Uint32 lastFrame = 0;
constexpr Uint32 frameTimeLimit = 1000 / 60;

constexpr Uint32 fpsMeasureIntervalFrames = 100;
Uint32 lastFpsMeasureTime = 0;
Uint32 framesSinceFpsMeasure = 0;

void ensureSDLInitialized() {
	if(screen) return;
	
	if(SDL_Init(SDL_INIT_VIDEO)) {
		std::cerr << "Couldn't initialize SDL.\n";
		exit(1);
	}
	
	atexit(SDL_Quit);
	
	screen = SDL_SetVideoMode(1024, 768, 32, SDL_SWSURFACE);
	if(screen == nullptr) {
		std::cerr << "Couldn't set video mode.\n";
		exit(1);
	}
}

struct PieceInfo {
	// Position/direction functions by lane.
	std::map<int, std::function<Vec2(double)>> pos;
	std::map<int, std::function<Vec2(double)>> dir;
	
	// Draw function by lane. draw(a, b) draws with transformation az + b.
	std::map<int, std::function<void(Vec2, Vec2)>> draw;
	
	Vec2 posAfter;
	Vec2 dirAfter;
};

PieceInfo straightPieceInfo(
	Piece piece,
	const std::vector<Lane>& lanes,
	Vec2 pos, Vec2 dir
) {
	PieceInfo ret;
	
	double length = piece.straight.length;
	Vec2 end = pos + length * dir;
	
	for(int lanei = 0; lanei < (int)lanes.size(); ++lanei) {
		Lane lane = lanes[lanei];
		
		Vec2 offset = Vec2(0, 1) * dir * lane.distanceFromCenter;
		Vec2 laneStart = pos + offset;
		Vec2 laneEnd = end + offset;
		
		ret.draw[lanei] = [=](Vec2 a, Vec2 b) {
			Vec2 p1 = a * laneStart + b;
			Vec2 p2 = a * laneEnd + b;
			lineColor(screen, p1.real(), p1.imag(), p2.real(), p2.imag(), 0x000000FF);
		};
		
		ret.pos[lanei] = [=](double x) {
			return laneStart + x / length * (laneEnd - laneStart);
		};
		
		ret.dir[lanei] = [=](double x) {
			return dir;
		};
	}
	
	ret.posAfter = end;
	ret.dirAfter = dir;
	
	return ret;
}

PieceInfo bendPieceInfo(
	Piece piece,
	const std::vector<Lane>& lanes,
	Vec2 pos, Vec2 dir
) {
	PieceInfo ret;
	
	double angle = piece.bend.angle;
	double radius = piece.bend.radius;
	double side = angle > 0 ? 1.0 : -1.0;
	
	Vec2 rotation(std::cos(angle), std::sin(angle));
	
	Vec2 toPos = Vec2(0, -1) * side * radius * dir;
	Vec2 toEnd = toPos * rotation;
	
	Vec2 center = pos - toPos;
	
	double angle1 = (180.0 / PI) * std::arg(toPos);
	double angle2 = (180.0 / PI) * std::arg(toEnd);
	if(angle < 0) std::swap(angle1, angle2);
	
	for(int lanei = 0; lanei < (int)lanes.size(); ++lanei) {
		Lane lane = lanes[lanei];
		double lane_radius = std::abs(toPos) - side * lane.distanceFromCenter;
		
		ret.draw[lanei] = [=](Vec2 a, Vec2 b) {
			Vec2 p = a * center + b;
			double angle_offset = (180.0 / PI) * std::arg(a);
			arcColor(
				screen,
				p.real(), p.imag(), lane_radius * std::abs(a),
				angle1 + angle_offset, angle2 + angle_offset,
				0x000000FF
			);
		};
		
		auto relativePosition = [=](double x) {
			double rel_angle = side * x / lane_radius;
			Vec2 rel_rotation =
				Vec2(std::cos(rel_angle), std::sin(rel_angle));
			Vec2 d = rel_rotation * toPos;
			d /= std::abs(d);
			return d;
		};
		
		ret.pos[lanei] = [=](double x) {
			return center + lane_radius * relativePosition(x);
		};
		
		ret.dir[lanei] = [=](double x) {
			return Vec2(0, side) * relativePosition(x);
		};
	}
	
	ret.posAfter = center + toEnd;
	ret.dirAfter = dir * rotation;
	
	return ret;
}

} // namespace

void drawGameState(const Track& track, const GameState& gameState) {
	ensureSDLInitialized();
	
	Uint32 ticksNow = SDL_GetTicks();
	Uint32 nextFrame = lastFrame + frameTimeLimit;
	if(ticksNow < nextFrame) {
		SDL_Delay(nextFrame - ticksNow);
	}
	lastFrame = nextFrame;
	
	++framesSinceFpsMeasure;
	if(framesSinceFpsMeasure == fpsMeasureIntervalFrames) {
		std::cout << "FPS: " << (1000.0 * fpsMeasureIntervalFrames) / ((double)ticksNow - (double)lastFpsMeasureTime) << "\n";
		framesSinceFpsMeasure = 0;
		lastFpsMeasureTime = ticksNow;
	}
	
	SDL_Event event;
	while(SDL_PollEvent(&event)) {
		if(event.type == SDL_QUIT) exit(0);
		if(event.type == SDL_KEYDOWN) {
			switch(event.key.keysym.sym) {
				case SDLK_ESCAPE: exit(0); break;
				case SDLK_WORLD_7: interactiveThrottle = 0.0; break;
				case SDLK_1: interactiveThrottle = 0.1; break;
				case SDLK_2: interactiveThrottle = 0.2; break;
				case SDLK_3: interactiveThrottle = 0.3; break;
				case SDLK_4: interactiveThrottle = 0.4; break;
				case SDLK_5: interactiveThrottle = 0.5; break;
				case SDLK_6: interactiveThrottle = 0.6; break;
				case SDLK_7: interactiveThrottle = 0.7; break;
				case SDLK_8: interactiveThrottle = 0.8; break;
				case SDLK_9: interactiveThrottle = 0.9; break;
				case SDLK_0: interactiveThrottle = 1.0; break;
				default: break;
			}
		}
	}
	
	SDL_FillRect(screen, nullptr, SDL_MapRGB(screen->format, 255, 255, 255));
	
	Vec2 pos(0, 0);
	Vec2 dir(1, 0);
	
	std::vector<PieceInfo> pieces(track.pieces.size());
	
	for(std::size_t piecei = 0; piecei < track.pieces.size(); ++piecei) {
		Piece piece = track.pieces[piecei];
		
		if(piece.type == Piece::STRAIGHT) {
			pieces[piecei] = straightPieceInfo(piece, track.lanes, pos, dir);
			pos = pieces[piecei].posAfter;
			dir = pieces[piecei].dirAfter;
		} else if(piece.type == Piece::BEND) {
			pieces[piecei] = bendPieceInfo(piece, track.lanes, pos, dir);
			pos = pieces[piecei].posAfter;
			dir = pieces[piecei].dirAfter;
		}
	}
	
	Vec2 a = Vec2(0.8, 0.3);
	Vec2 b = Vec2(600, 200);
	
	for(const PieceInfo& piece : pieces) {
		for(const auto& p : piece.draw) p.second(a, b);
	}
	
	for(CarState car : gameState.cars) {
		if(car.piece < 0 || car.piece >= (int)track.pieces.size()) continue;
		Vec2 pos = pieces[car.piece].pos[car.fromLane](car.inPieceDistance);
		Vec2 dir = pieces[car.piece].dir[car.fromLane](car.inPieceDistance);
		double drift_angle = car.angle;
		dir *= Vec2(std::cos(drift_angle), std::sin(drift_angle));
		Vec2 drawpos = a * pos + b;
		Vec2 drawdir1 = a * dir;
		Vec2 drawdir2 = Vec2(0, 1) * a * dir;
		// TODO: use actual size of the car. Clean up.
		Vec2 v1 = drawpos + 10.0 * drawdir1 + 4.0 * drawdir2;
		Vec2 v2 = drawpos - 10.0 * drawdir1 + 4.0 * drawdir2;
		Vec2 v3 = drawpos - 10.0 * drawdir1 - 4.0 * drawdir2;
		Vec2 v4 = drawpos + 10.0 * drawdir1 - 4.0 * drawdir2;
		Sint16 vx[] = {(Sint16)v1.real(), (Sint16)v2.real(), (Sint16)v3.real(), (Sint16)v4.real()};
		Sint16 vy[] = {(Sint16)v1.imag(), (Sint16)v2.imag(), (Sint16)v3.imag(), (Sint16)v4.imag()};
		
		Uint32 color = car.crashed ? 0x666666FF : 0xFF0000FF;
		filledPolygonColor(screen, vx, vy, 4, color);
	}
	
	SDL_Flip(screen);
}

double getInteractiveThrottle() {
	return interactiveThrottle;
}

#endif
