#include "fake_server.h"

#include "debug_draw.h"

#include <cassert>
#include <vector>

void runFakeServer(const std::vector<std::unique_ptr<Bot>>& bots, const Track& track) {
	assert(!track.pieces.empty());
	
	int carCount = bots.size();
	
	GameState gameState;
	gameState.cars.resize(carCount);
	int lane = 0;
	double dist = 0.0;
	for(CarState& carState : gameState.cars) {
		carState.name = "test car";
		carState.color = "red";
		carState.angle = 0;
		carState.inPieceDistance = dist;
		carState.piece = 0;
		carState.fromLane = lane;
		carState.toLane = lane;
		carState.lap = 0;
		
		++lane;
		if(lane == (int)track.lanes.size()) {
			dist += 40.0;
			lane = 0;
		}
	}
	
	for(const std::unique_ptr<Bot>& bot : bots) {
		bot->initGame(track);
	}
	
	std::vector<double> speed(carCount, 0.0);
	
	// 0 if not crashed, otherwise counting down.
	std::vector<int> crashCounter(carCount, 0);
	
	while(true) {
#ifdef DEBUG_DRAW
		drawGameState(track, gameState);
#endif
		
		for(int i = 0; i < carCount; ++i) {
			Bot::Move move = bots[i]->run(gameState, i);
			CarState& carState = gameState.cars[i];
			
			// Do physics.
			if(crashCounter[i]) {
				--crashCounter[i];
			} else {
				carState.inPieceDistance += speed[i];
				speed[i] += 0.1 * (move.throttle - 0.3);
				speed[i] -= 0.0006 * speed[i] * speed[i];
				if(speed[i] < 0) speed[i] = 0;
				double invradius = track.getLanePieceInverseSignedRadius(
					carState.fromLane,
					carState.piece
				);
				carState.angle -= 0.4 * speed[i] * speed[i] * invradius;
				carState.angle *= 0.9;
				
				if(std::abs(carState.angle) > 1.0) {
					crashCounter[i] = 600;
					speed[i] = 0;
					carState.angle = 0;
				}
			}
			carState.crashed = (bool)crashCounter[i];
			
			double lane_length = track.getLanePieceLength(carState.fromLane, carState.piece);
			while(carState.inPieceDistance >= lane_length) {
				++carState.piece;
				carState.inPieceDistance -= lane_length;
				if(carState.piece >= (int)track.pieces.size()) {
					carState.piece = 0;
				}
				lane_length = track.getLanePieceLength(carState.fromLane, carState.piece);
			}
		}
	}
}
