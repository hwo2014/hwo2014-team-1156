#include "RulesModel.hpp"
#include "GameState.hpp"
#include "Matrix.hpp"
#include "Track.hpp"
#include "geometry.hpp"

const double STEP_TIME = 0.02;
const double ACCELERATION = 0.05;
const double SLOWDOWN = 0.05;
const double TURN_SPEED = 1.0 / 7.0;
const double CAR_SIZE = 2;

GameState RulesModel::step(const Track& track, GameState st) const {
	for(CarState& c: st.cars) {
		double oldVel = c.velocity;
		c.velocity += STEP_TIME * ACCELERATION * c.throttle;
		c.velocity *= pow(1 - SLOWDOWN, STEP_TIME);
		c.inPieceDistance += .5 * STEP_TIME * (oldVel + c.velocity);
		track.normalizePosition(c);
	}
	return st;
}

bool RulesModel::end(GameState) const {
	return 0;
}
