#include "Track.hpp"
#include "draw.hpp"
#include "RulesModel.hpp"
#include "Vector.hpp"
#include "GameState.hpp"
#include "flags.hpp"
#include "Bot.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
using namespace std;

FLAG(track, string, "");
FLAG(draw, bool, true);
FLAG(players, int, 2);

Track readTrack(istream& in) {
	Track track;
	char type;
	while(in>>type) {
		Piece piece;
		piece.type = type == 'S' ? Piece::STRAIGHT : Piece::BEND;
		if (piece.type == Piece::STRAIGHT) {
			in >> piece.straight.length;
		} else {
			in >> piece.bend.radius >> piece.bend.angle;
			piece.bend.angle *= M_PI / 180;
		}
		in >> piece.canSwitch;
		track.pieces.push_back(piece);
	}
	cout<<"track pieces: "<<track.pieces.size()<<'\n';
	return track;
}
Track getTrack() {
	ifstream in(flag::track);
	assert(in);
	return readTrack(in);
}

void runLocal(vector<Bot>& bots) {
	Track track = getTrack();
	track.lanes.push_back(0);
	GameState state;
	for(int i=0; i<(int)bots.size(); ++i) {
		CarState s;
		s.inPieceDistance = -5 - 10*i;
//		cout<<"k "<<i<<' '<<s.inPieceDistance<<'\n';
		track.normalizePosition(s);
		state.cars.push_back(s);
	}
	RulesModel rules;
	while(!rules.end(state)) {
		for(size_t i=0; i<bots.size(); ++i) {
			auto move = bots[i].run(state);
			state.cars[i].throttle = move.throttle;
			state.cars[i].switchLane = move.switchLane;
		}
		state = rules.step(track, state);

		if (flag::draw) {
			handleFrame(track, state);
		}
	}
}

int main(int argc, char* argv[]) {
	flag::parseFlags(argc, argv);
	if (flag::draw) {
		initDraw();
	}
	vector<Bot> drivers(flag::players);
	runLocal(drivers);
}
