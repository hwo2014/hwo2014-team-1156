#pragma once

#ifdef REAL_TYPE
typedef REAL_TYPE Real;
#else
typedef double Real;
#endif
