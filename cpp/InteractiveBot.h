#pragma once
#include "Bot.h"
#include "GameState.h"
#include "debug_draw.h"

#include <cstdlib>

class InteractiveBot : public Bot {
public:
	InteractiveBot() {
#ifndef DEBUG_DRAW
		std::cerr << "Interactive bot can only be used with debug draw.\n";
		exit(1);
#endif
	}
	
	virtual void initGame(const Track& track) override {
	}
	virtual Move run(const GameState& gameState, int myIndex) override {
		double throttle = 0.0;
#ifdef DEBUG_DRAW
		throttle = getInteractiveThrottle();
#endif
		return {throttle, 0};
	}
};
