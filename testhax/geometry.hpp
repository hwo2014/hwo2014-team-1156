#pragma once

#include "Vector.hpp"
#include <cassert>
#include <utility>

inline bool isCCW(const Vec2& a, const Vec2& b, const Vec2& c) {
	return cross(b - a, c - a) > 0.0;
}

inline bool intersects(const Vec2& a1, const Vec2& a2, const Vec2& b1, const Vec2 &b2) {
	return isCCW(a1, a2, b1) != isCCW(a1, a2, b2);
}
inline bool segmentLineIntersects(const Vec2& s1, const Vec2& s2, const Vec2& l1, const Vec2& l2) {
	Vec2 dir = l2-l1;
	return cross(dir, s1-l1) * cross(dir, s2-l1) < 0;
}
inline bool segmentsIntersect(const Vec2& a1, const Vec2& a2, const Vec2& b1, const Vec2& b2) {
	return segmentLineIntersects(a1, a2, b1, b2)
		&& segmentLineIntersects(b1, b2, a1, a2);
}

inline Real intersectionY(const Vec2& a1, const Vec2& a2, const Vec2& b1, const Vec2 &b2) {
	Real x = a1[0];
	Real y = a1[1];
	Real X = a2[0];
	Real Y = a2[1];
	Real u = b1[0];
	Real v = b1[1];
	Real U = b2[0];
	Real V = b2[1];
	
	return ((-U*v+u*V)*(y-Y)-(v-V)*(-X*y+x*Y))/(-(v-V)*(x-X)+(u-U)*(y-Y));
}

/// Rotate v 90° CCW.
inline Vec2 rotate90(const Vec2& v) {
	return Vec2(-v[1], v[0]);
}

inline Real mag(Vec2 v) {
	return norm(v);
}
inline Real mag2(Vec2 v) {
	return norm2(v);
}

inline Real segPointDist(Vec2 a, Vec2 b, Vec2 pt) {
	Vec2 dir = b-a;
	Vec2 ap = pt-a;
	if (dot(ap,dir)<0) return mag(ap);
	Vec2 bp = pt-b;
	if (dot(bp,dir)>0) return mag(bp);

	Real res = cross(dir, ap) / mag(dir);
	return res<0 ? -res : res;
}

/// Returns the rightmost intersection point when looking from a towards b if side=-1.
/// If side=1 returns the leftmost intersection.
#if 0
inline Vec2 circleIntersection(Vec2 a, Vec2 b, Real r, Real side) {
	Vec2 dir = .5*(b-a);
	Vec2 normal = rotate90(dir);
	Real dist2 = mag2(dir);
	assert(r*r >= dist2);
	Real x = sqrt((r*r - dist2) / dist2);
	return a + dir + side*x*normal;
}
#endif
inline Vec2 circleIntersection(Vec2 a, Vec2 b, Real ra, Real rb, Real side) {
	Vec2 dir = b-a;
	Real d2 = mag2(dir);
	Real d = sqrt(d2);
	Real x = (d2 - rb*rb + ra*ra) / (2*d);
	Real y2 = ra*ra - x*x;
	if (y2<0) {
		if (y2>=-1e-3) y2=0;
		assert(y2 >= 0);
	}
	Real y = sqrt(y2);
	Vec2 dir0 = dir/d;
	Vec2 normal = rotate90(dir0);
	return a + x*dir0 + side*y*normal;
}
inline std::pair<Real,Real> polyroot(Real a, Real b, Real c, bool* ok=0) {
	Real D = b*b - 4*a*c;
//	std::cout<<"D "<<D<<'\n';
	if (D<0) {
		if (ok) *ok = 0;
		return std::make_pair(0,0);
	}
	if (ok) *ok = 1;
	Real d = sqrt(D);
//	std::cout<<"asd "<<a<<' '<<b<<' '<<d<<'\n';
	return std::make_pair((-d-b) / (2*a), (d-b) / (2*a));
}
/// Returns the earliest intersection of line (a,b) with circle (mid,r)
/// when looking from a towards b.
inline Vec2 lineCircleIntersection(Vec2 a, Vec2 b, Vec2 mid, Real r, bool* ok=0) {
	Vec2 dir = b-a;
	Real d2 = mag2(dir);
	if (d2 < 1e-6) {
		if (ok) *ok = fabs(mag2(mid-a)-r)<1e-5;
		return a;
	}
	Real x = polyroot(d2, 2*dot(dir, a-mid), mag2(a-mid) - r*r, ok).first;
//	std::cout<<"root: "<<x<<'\n';
	return a + x*dir;
}

/// Returns the intersection of the circle with the tangent of the
/// circlethat goes trough pt.
/// Returns the rightmost solution when looking from pt towards center.
inline Vec2 pointCircleLine(Vec2 pt, Vec2 center, Real r, Real side=-1) {
	Vec2 mid = .5*(pt+center);
	Vec2 res = circleIntersection(mid, center, mag(mid-pt), r, side);
//	std::cout<<"dists: "<<mag(res-mid)<<' '<<r<<' '<<mag(res-center)<<' '<<mag(mid-pt)<<'\n';
	return res;
}

inline Vec2 lineIntersection(Vec2 a, Vec2 b, Vec2 c, Vec2 d) {
	Vec2 v = a-b, u = c-d;
	Real c1 = cross(a,b), c2 = cross(c,d);
	Real denom = cross(v, u);
	Real x = c1 * u[0] - c2 * v[0];
	Real y = c1 * u[1] - c2 * v[1];
	return Vec2(x,y) / denom;
}
inline bool isInRange(Vec2 start, Vec2 end, Vec2 pt) {
	Vec2 dir = end-start;
	return dot(dir, pt-start)>=0 && dot(dir, pt-end)<=0;
}
