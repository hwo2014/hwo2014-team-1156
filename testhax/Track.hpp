#pragma once
#include <vector>
#include <cmath>
#include "VecBase.hpp"
struct CarState;

struct Piece {
	enum Type { STRAIGHT, BEND } type;
	union {
		struct {
			Real length;
		} straight;
		struct {
			Real radius;
			Real angle;
		} bend;
	};
	bool canSwitch;

	double size(double offset) const {
		if (type == STRAIGHT) return straight.length;
		return fabs(bend.angle) * (bend.radius + offset);
	}
};

struct CarType {
	double length, width, guideFlag;
};

struct Track {
	std::vector<Piece> pieces;
	std::vector<Real> lanes;
	void normalizePosition(CarState& pos) const;
#if 0
	Vec2 startPosition;
	double startAngle;
#endif
};
