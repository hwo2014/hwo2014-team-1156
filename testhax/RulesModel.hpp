#pragma once
struct GameState;
struct Track;

struct RulesModel {
	GameState step(const Track& track, GameState st) const;
	bool end(GameState st) const;
};
