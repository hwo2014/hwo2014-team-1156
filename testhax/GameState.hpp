#pragma once
#include "common.hpp"
#include <vector>

struct CarState {
	Real angle = 0;
	Real inPieceDistance = 0;
	int piece = 0;
	int fromLane = 0;
	int toLane = 0;

	Real velocity = 0;
	Real laneChangeState = 0;
	Real angularVelocity = 0;

	Real throttle = 0;
	int switchLane = 0;
};

struct GameState {
	std::vector<CarState> cars;
};
