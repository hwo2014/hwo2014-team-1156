#pragma once
struct GameState;

struct Bot {
	struct Move {
		double throttle;
		int switchLane;
	};
	Move run(const GameState& state);
};
