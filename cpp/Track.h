#pragma once

#include <iostream>
#include <jsoncons/json.hpp>

struct Piece {
	static Piece fromJSON(const jsoncons::json& src);
	
	enum Type { STRAIGHT, BEND } type;
	union {
		struct {
			double length;
		} straight;
		struct {
			double radius;
			double angle;
		} bend;
	};
	bool canSwitch;

	double size(double offset) const {
		if (type == STRAIGHT) return straight.length;
		return fabs(bend.angle) * (bend.radius + offset);
	}
};

struct Lane {
	static Lane fromJSON(const jsoncons::json& src);
	
	double distanceFromCenter;
	int server_index;
};

struct Track {
	static Track fromJSON(const jsoncons::json& src);
	
	int getLaneIndexByServerIndex(int server_index) const;
	
	double getLanePieceLength(int lane, int piece) const;
	double getLanePieceInverseSignedRadius(int lane, int piece) const;
	
	double getDistance(int lane, int piece1, double pos1, int piece2, double pos2) const;
	
	// Sorted by distance from center.
	std::vector<Lane> lanes;
	
	std::vector<Piece> pieces;
};
