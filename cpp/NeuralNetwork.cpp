#include "NeuralNetwork.h"
#include <cassert>
#include <cmath>
#include <random>

typename NeuralNetwork::Vector NeuralNetwork::multMatVec(
		const Matrix& m, const Vector& v) {
	Vector res(m[0].size());
	assert(m.size() == v.size());
	for(size_t i=0; i<m.size(); ++i) {
		res += m[i] * v[i];
	}
	return res;
}

double NeuralNetwork::activation(double x) {
//	return x;
	return 1 / (1 + exp(-x));
}
double NeuralNetwork::activationDerivate(double x) {
	double y = activation(x);
//	return 1;
	return y * (1-y);
}

FunctionApproximator::Vector NeuralNetwork::evaluate(Vector input) {
	Vector x = input;
	for(size_t i=0; i<weights.size(); ++i) {
		x = multMatVec(weights[i], x);
		if (i+1<weights.size()) for(double& y: x) y = activation(y);
	}
	return x;
}

void NeuralNetwork::teach(Vector input, Vector output, double learningRate) {
	std::vector<Vector> layerResults;
	Vector x = input;
	layerResults.push_back(x);
	for(size_t i=0; i<weights.size(); ++i) {
		x = multMatVec(weights[i], x);
		layerResults.push_back(x);
		if (i+1<weights.size()) for(double& y: x) y = activation(y);
	}

	Vector deltas(x.size());

	for(size_t i=0; i<x.size(); ++i) {
		deltas[i] = (output[i] - x[i]);// * activationDerivate(x[i]);
	}

	for(int i=(int)weights.size()-1; i>=0; --i) {
		Vector ndeltas(weights[i].size());
		for(size_t j=0; j<weights[i].size(); ++j) {
			assert(deltas.size() == weights[i][j].size());
			double z = layerResults[i][j];
			double y = activation(z);
			double e = 0;
			for(size_t k=0; k<weights[i][j].size(); ++k) {
				e += weights[i][j][k] * deltas[k];
			}
			if (i>0) e *= activationDerivate(z);
			ndeltas[j] = e;
			for(size_t k=0; k<weights[i][j].size(); ++k) {
				weights[i][j][k] += learningRate * deltas[k] * y;
			}
		}
		deltas = ndeltas;
	}
}

template<class R>
typename NeuralNetwork::Matrix NeuralNetwork::randomMatrix(int from, int to, R& r) {
	Matrix m(from, Vector(to));
	for(int a=0; a<from; ++a) for(int b=0; b<to; ++b)
		m[a][b] = 2.0 * r() / r.max() - 1;
	return m;
}

NeuralNetwork NeuralNetwork::createRandom(std::vector<int> layerSizes) {
	static std::mt19937 rng;
	NeuralNetwork res;
	int n = layerSizes.size();
	for(int i=0; i+1<n; ++i)
		res.weights.push_back(randomMatrix(layerSizes[i], layerSizes[i+1], rng));
	return res;
}
