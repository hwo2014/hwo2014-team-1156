#include "flags.hpp"
#include <unordered_map>
#include <string>
#include <cassert>
#include <sstream>
using namespace std;

namespace flag {
namespace {
unordered_map<string, ParserBase*> parsers;
}
void addFlagParser(const char* name, ParserBase* parser) {
	parsers[string(name)] = parser;
}
void parseFlags(int argc, char* argv[]) {
	for(int i=1; i<argc; ++i) {
		string s(argv[i]);
		s = s.substr(s.find_first_not_of('-'));
		ParserBase* parser = parsers[s];
		assert(parser);
		istringstream in(argv[++i]);
		parser->parse(in);
	}
}
}
