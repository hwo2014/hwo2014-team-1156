#include "Track.hpp"
#include "GameState.hpp"
#include <cassert>
#include <iostream>
using namespace std;

void Track::normalizePosition(CarState& pos) const {
	int piece = pos.piece % pieces.size();
	assert(pos.fromLane>=0 && pos.fromLane<(int)lanes.size());
	assert(pos.toLane>=0 && pos.toLane<(int)lanes.size());
	double dist = pos.inPieceDistance;
	double flane = lanes[pos.fromLane], tlane = lanes[pos.toLane];
	double offset = flane + pos.laneChangeState * (tlane - flane);
	while(dist < 0) {
		piece--;
		if (piece<0) piece += pieces.size();
		dist += pieces[piece].size(offset);
	}
	while(1) {
		double size = pieces[piece].size(offset);
//		cout<<"asd "<<size<<' '<<dist<<'\n';
		assert(size > 0);
		if (dist < size) break;
		dist -= size;
		++piece;
		if (piece>=(int)pieces.size()) piece -= pieces.size();
	}
	pos.piece = piece;
	pos.inPieceDistance = dist;
}
